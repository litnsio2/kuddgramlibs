
add_library(Qt5::PollPositionPlugin MODULE IMPORTED)

_populate_Positioning_plugin_properties(PollPositionPlugin RELEASE "position/qtposition_positionpoll.lib")
_populate_Positioning_plugin_properties(PollPositionPlugin DEBUG "position/qtposition_positionpolld.lib")

list(APPEND Qt5Positioning_PLUGINS Qt5::PollPositionPlugin)
