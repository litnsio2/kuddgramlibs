
add_library(Qt5::QTcpServerConnection MODULE IMPORTED)

_populate_Declarative_plugin_properties(QTcpServerConnection RELEASE "qml1tooling/qmldbg_tcp_qtdeclarative.lib")
_populate_Declarative_plugin_properties(QTcpServerConnection DEBUG "qml1tooling/qmldbg_tcp_qtdeclaratived.lib")

list(APPEND Qt5Declarative_PLUGINS Qt5::QTcpServerConnection)
